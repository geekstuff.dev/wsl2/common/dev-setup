#!make

MAKEFLAGS += --no-print-directory

# Detect linux environment
LX_TYPE = $(shell \
	(test -e /etc/alpine-release && echo alpine) \
	|| (test -e /etc/lsb-release && grep -q "DISTRIB_ID=Ubuntu" /etc/lsb-release && ( \
		(grep -q "DISTRIB_RELEASE=24.04" /etc/lsb-release && ( \
			(pidof systemd 2>/dev/null 1>/dev/null && echo "ubuntu-2404-systemd") \
			|| echo "ubuntu-2404" \
		)) \
		|| (grep -q "DISTRIB_RELEASE=22.04" /etc/lsb-release && ( \
			(pidof systemd 2>/dev/null 1>/dev/null && echo "ubuntu-2204-systemd") \
			|| echo "ubuntu-2204" \
		)) \
		|| (grep -q "DISTRIB_RELEASE=20.04" /etc/lsb-release && echo "ubuntu-2004") \
	)) \
)
LX_DISTRO = $(shell echo $(LX_TYPE) | cut -d"-" -f1)
LX_BASE = $(shell \
	(test -e /etc/alpine-release && echo alpine) \
	|| (test -e /etc/lsb-release && grep -q "DISTRIB_ID=Ubuntu" /etc/lsb-release && echo "debian") \
)

# Let (dockerfile) provide a value, or assume it's a user running this from his wsl imported image
SYSD_VARIANT ?= $(shell pidof systemd 1>/dev/null 2>/dev/null && echo "systemd" || echo "non-systemd")

# The command info and examples will use to show users
CLI_COMMAND ?= docker-dev

all: .check info

include parts/*/*.mk parts/*.mk
include utilities/*.mk

# NOTE: For now it's ok if this one is called too many times
.PHONY: .check
.check:
	@$(MAKE) .check-wsl 1>/dev/null 2>/dev/null \
		|| $(MAKE) .check-docker-env 1>/dev/null 2>/dev/null \
		|| { echo "could not detect a Docker or WSL2 environment" 1>&2; false; }
	@$(MAKE) .check-lx-type 1>/dev/null 2>/dev/null || { \
		echo "For now this only handles Ubuntu 20.04, 22.04, 22.04 + systemd, 24.04 + systemd and Alpine."; \
		echo "error: cannot handle this specific scenario."; \
		false; \
	}
	@! test -e /workspace/common/dev-setup || { \
		echo "This Makefile is NOT meant to be executed from wsl2/common/devcontainer."; \
		false; \
	}

.PHONY: .check-wsl
.check-wsl:
	@test -n "${WSL_DISTRO_NAME}"

.PHONY: .check-docker-env
.check-docker-env:
	@test -e /.dockerenv || test -n "${DOCKER_DEV_SERVICES}"

.PHONY: .check-lx-type
.check-lx-type:
	@test -n "${LX_TYPE}"

.PHONY: info
info: .check
	$(info Geekstuff.dev / WSL2 / Common / Dev-Setup - running in ${LX_TYPE})
	$(info )
	$(info > ${CLI_COMMAND} info                this help message)
	$(info > ${CLI_COMMAND} gpg                 info on how to enable git commit signing with gpg)
	$(info > ${CLI_COMMAND} k8                  utilities around kubernetes (cli tools, local clusters, more))
	$(info > ${CLI_COMMAND} static-dns          info on setting static dns in WSL and reducing issues with VPN)
	$(info > ${CLI_COMMAND} file-sharing        utility to setup shared assets and folders between WSL distributions)
	$(info > ${CLI_COMMAND} docker-flush        clears all docker assets like volumes, containers, images, networks, etc.)
	$(info > ${CLI_COMMAND} devcontainer        info on how this WSL distribution supports devcontainers)
	$(info > ${CLI_COMMAND} vscode              opens docker dev project in VSCode)
	@:

.PHONY: .detected-env
.detected-env:
	$(info Running for ${LX_DISTRO} / ${LX_TYPE} / ${SYSD_VARIANT})
	@:

.PHONY: .build-common
.build-common: .detected-env .check

.PHONY: 00-basics
00-basics: .build-common .00-basics

.PHONY: 10-template-user
10-template-user: .build-common .10-template-user

.PHONY: 20-docker
20-docker: .build-common .20-docker

.PHONY: 30-wsl-conf
30-wsl-conf: .build-common .30-wsl-conf

.PHONY: 40-user-config
40-user-config: .build-common .40-user-config

.PHONY: 50-wsl-services
50-wsl-services: .build-common .50-wsl-services

.PHONY: 99-post-import
99-post-import: .build-common .99-post-import
