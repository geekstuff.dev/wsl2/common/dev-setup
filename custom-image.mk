#!make

# NOTE: This Makefile is meant to be included by WSL images in their gitlab-ci pipeline.
#       It is not meant to be included by this project itself.

# hide entering/leaving directory messages
MAKEFLAGS += --no-print-directory

# To define in the makefile that includes this one
# SOURCE_IMAGE ?= alpine
# SOURCE_TAG ?= 3.20
# TITLE = Alpine

#
PROJECT_TAG ?= v0.0.0
BUILD_REF ?= ${PROJECT_TAG}
BUILD_SOURCE ?= http://localhost
SYSD_VARIANT ?= non-systemd

#
PACKAGE_DIR = packages
DOCKER_IMAGE ?= wsl-docker-dev/${SOURCE_IMAGE}/${SOURCE_TAG}$(shell test "${SYSD_VARIANT}" = "systemd" && echo "/${SYSD_VARIANT}")
PACKAGE_NAME ?= wsl-docker-dev-${SOURCE_IMAGE}-${SOURCE_TAG}$(shell test "${SYSD_VARIANT}" = "systemd" && echo "-${SYSD_VARIANT}")

# used by docker-dev images to know where to load this file from depending if
# in a devcontainer or a CI pipeline
MAKE_LIB ?= yes

# used in dev
WSL_USER ?= wsluser

# next block is to figure the windows import path when testing packages
DD_PROJECT = $(shell basename $(shell pwd))
DD_PROJECT_SLUG = $(shell echo ${DD_PROJECT} | sed -e 's/\.//g')
WSL_PATH_ADJUSTED ?= $(shell echo ${WSL_PATH} | rev | cut -d'/' -f3- | rev)
WSL_PATH_DD_PROJECT ?= ${WSL_PATH_ADJUSTED}/docker-dev/${DD_PROJECT}
WIN_PATH_TEST_PACKAGE ?= \\\\wsl$$\\${WSL_DISTRO_NAME}$(shell echo ${WSL_PATH_DD_PROJECT} | sed 's|/|\\\\|g')\\packages\\${PACKAGE_NAME}.tar.gz

all: info

.PHONY: info
info:
	$(info Geekstuff.dev / WSL2 / ${TITLE})
	$(info > make build)
	$(info > make package)
	$(info > make shell)
	@:

# If in a devcontainer, dev-setup gets copied. Otherwise pipelines are
# expected to handle that before calling this.
.PHONY: build
build: .dev-setup.copy
	@test -e dev-setup/Makefile || { echo "Cannot find dev-setup project"; false; }
	docker build \
		-t ${DOCKER_IMAGE} \
		--build-arg "BUILD_REF=${BUILD_REF}" \
		--build-arg "BUILD_SOURCE=${BUILD_SOURCE}" \
		--build-arg "SOURCE_IMAGE=${SOURCE_IMAGE}" \
		--build-arg "SOURCE_TAG=${SOURCE_TAG}" \
		.
	@echo "Built image: [${DOCKER_IMAGE}]"
	@$(MAKE) .dev-setup.cleanup

.PHONY: package
package: .pre-package
	@mkdir -p ${PACKAGE_DIR}
	docker export ${PACKAGE_NAME}-instance | gzip -c > ${PACKAGE_DIR}/${PACKAGE_NAME}.tar.gz
	@printf "Package size:\n  "
	@du -hs ${PACKAGE_DIR}/${PACKAGE_NAME}.tar.gz
	@$(MAKE .docker.cleanup)
	@! test -d /workspace/common/dev-setup || ( \
		echo ""; \
		echo "Example import, setup, launch:"; \
		echo "  wsl --import tst-${DD_PROJECT_SLUG} .\wsl-tst-${DD_PROJECT_SLUG} ${WIN_PATH_TEST_PACKAGE}"; \
		echo "  wsl -d tst-${DD_PROJECT_SLUG} post-import-setup"; \
		echo "  wsl ~ -d tst-${DD_PROJECT_SLUG}"; \
		echo ""; \
		echo "Example cleanup:"; \
		echo "  wsl --terminate tst-${DD_PROJECT_SLUG}"; \
		echo "  wsl --unregister tst-${DD_PROJECT_SLUG}"; \
		echo "  rm wsl-tst-${DD_PROJECT_SLUG}"; \
		echo ""; \
	)

.PHONY: shell
shell:
	@echo "Launching a shell in an ephemeral docker instance."
	@echo "Things to try:"
	@echo "- post-import-setup"
	@echo "- sudo -u \$$WSL_USER -- bash -l"
	docker run --rm -it ${DOCKER_IMAGE}

.PHONY: shell-user
shell-user:
	docker run --rm -it ${DOCKER_IMAGE} sudo -u ${WSL_USER} -- bash -l

.PHONY: .pre-package
.pre-package: .docker.cleanup
	docker run --name ${PACKAGE_NAME}-instance ${DOCKER_IMAGE}

.PHONY: .docker.cleanup
.docker.cleanup:
	@docker stop ${PACKAGE_NAME}-instance 1>/dev/null 2>/dev/null || true
	@docker rm ${PACKAGE_NAME}-instance 1>/dev/null 2>/dev/null || true

.PHONY: .dev-setup.copy
.dev-setup.copy: .dev-setup.cleanup
	@! test -d /workspace/common/dev-setup || ( \
		mkdir -p dev-setup \
		&& cp -R /workspace/common/dev-setup/. dev-setup \
		&& echo "Copy dev-setup completed" \
	)

.PHONY: .dev-setup.cleanup
.dev-setup.cleanup:
	@! test -d /workspace/common/dev-setup || ( \
		rm -rf dev-setup \
		&& echo "Cleaned up dev-setup" \
	)
