#!/bin/sh

set -e

dir="$(dirname "$0")"

if test $(id -u) -ne 0; then
    echo "root is needed to run this"
    exit 1
fi

if ! test -e /usr/local/bin/docker-dev; then
    ln -s ${dir}/docker-dev.sh /usr/local/bin/docker-dev
fi

if ! (command -v bash && command -v make && command -v sudo); then
    if command -v apk 2>/dev/null 1>/dev/null; then
        apk --update --no-cache add bash make sudo
    elif command -v apt 2>/dev/null 1>/dev/null; then
        apt-get update \
            && apt-get -y install --no-install-recommends bash make sudo 2>&1 \
            && rm -rf /var/lib/apt/lists/*
    fi
fi
