#!/bin/sh

set -e

make --no-print-directory -C /usr/lib/docker-dev/ "$@"
