#!/bin/bash

# How to use
#
# Open a simple devcontainer (no multi-root, no workspaces):
#   devcontainer-open my/project/path
#   devcontainer-open geekstuff.dev/geekstuff.dev.gitlab.io/
#
# Open a multi-root devcontainer:
#   devcontainer-open my/project/path/some.code-workspace
#   devcontainer-open geekstuff.dev/wsl2/common/devcontainer/dev.code-workspace
#
# NOTES:
#   to reverse hex:
#     echo hex | sed 's/\([0-9A-F]\{2\}\)/\\\\\\x\1/gI' | xargs printf

set -e

# This block here helps with WSL non-systemd versions and
# also ensuring that ssh-agent and gpg-agent are present before
# devcontainer starts. (TODO confirm if that helps)
if test -z "$INTERNAL_RUN"; then
    INTERNAL_RUN=1 bash -l -c "$0 $1"
    exit $?
fi

WSLPrefix="wsl\$"
# Note: /mnt/wslg folder is only present in WSL preview version
if test -e /mnt/wslg; then
    WSLPrefix="wsl.localhost"
fi

detect_mode() {
    local path="$1"
    if test -d "$path" && test -f "$path/.devcontainer/devcontainer.json"; then
        echo "single"
    elif test -d "$path" && test -f "$path/.devcontainer.json"; then
        echo "singleatroot"
    elif test -f "$path" && [[ $path =~ .*\.code-workspace$ ]]; then
        echo "workspace"
    else
        echo "failed to detect devcontainer mode on [$path]" >&2
        return 1
    fi
}

handle_single() {
    local path=$(realpath "$1")
    local fullWinWslPath=$(echo "//${WSLPrefix}/${WSL_DISTRO_NAME}${path}" | sed 's|/|\\|g')
    local hex=$(printf "%s" "$fullWinWslPath" | od -A n -t x1 | sed 's/ *//g' | tr -d '\n')
    echo "fullWinWslPath: [$fullWinWslPath]"

    local dcJson="$2"
    test -e "$dcJson" || { echo "error: could not find $dcJson"; false; }

    local workspaceFolder
    if jq -e -r '.workspaceFolder' $dcJson 1>/dev/null 2>/dev/null; then
        workspaceFolder=$(jq -e -r '.workspaceFolder' $dcJson)
    else
        workspaceFolder="/workspaces/$(basename $path)"
    fi
    echo "workspaceFolder: [$workspaceFolder]"

    local folderURI="vscode-remote://dev-container+${hex//[[:space:]]/}${workspaceFolder}"

    # show and run
    echo code --folder-uri "$folderURI"
    code --folder-uri "$folderURI"
}

handle_workspace() {
    local file=$(realpath "$1")
    local folder="$(dirname $file)"
    local fullWinWslPath=$(echo "//${WSLPrefix}/${WSL_DISTRO_NAME}${file}" | sed 's|/|\\|g')
    local hex=$(printf "%s" "$fullWinWslPath" | od -A n -t x1 | sed 's/ *//g' | tr -d '\n')
    echo "fullWinWslPath: [$fullWinWslPath]"

    local dcJson="${folder}/.devcontainer/devcontainer.json"
    test -e "$dcJson" || { echo "error: could not find $dcJson"; false; }

    local workspaceFolder=$(cat "$dcJson" | jq -e -r '.workspaceFolder')
    (test -n "$workspaceFolder" || test "$workspaceFolder" = "null") \
        || { echo "error: could not find workspaceFolder value in $dcJson"; false; }
    echo "workspaceFolder: [$workspaceFolder]"

    # NOTE: Next workspaceFile variable assumes code-workspace files are next to .devcontainer/ folder.
    local workspaceFile=$(basename "$file")
    echo "workspaceFile: [$workspaceFile]"

    local fileURI="vscode-remote://dev-container+${hex//[[:space:]]/}${workspaceFolder}/${workspaceFile}"

    # show and run
    echo code --file-uri "$fileURI"
    code --file-uri "$fileURI"
}

## Main is below this point ##

path="$1"

# Note: with set -e above, a detect_mode failure (non-zero return) will halt the script
mode=$(detect_mode "$path")

# Handle modes
case $mode in
    single)
        handle_single "$path" "${path}/.devcontainer/devcontainer.json"
        ;;
    singleatroot)
        handle_single "$path" "${path}/.devcontainer.json"
        ;;
    workspace)
        handle_workspace "$path"
        ;;
    *)
        echo "mode [$mode] is not supported yet"
        exit 1
        ;;
esac

# Done. Add post notices to help out.

if test $(find $HOME/.ssh/ -name 'id_*' | wc -l) -eq 0; then
    echo ""
    echo "NOTES:"
    echo "  - No ssh keys are present. You might have issues using git pull/push/clone"
    echo "    if using the ssh protocol."
    echo "    Use \`ssh-keygen -t ed25519\` in a WSL shell session to generate a key pair."
    sleep 5
else
    if ! ssh-add -l 1>/dev/null 2>/dev/null; then
        echo ""
        echo "NOTES:"
        echo "  - Ssh keys are present but NOT loaded in the ssh-agent. You might have issues"
        echo "    using git pull/push/clone if using the ssh protocol."
        echo "    You can load them after the fact in another WSL shell session and using"
        echo "    \`ssh-add\`"
        sleep 5
    fi
fi
