#!/bin/bash

# Since non-systemd WSL does not clear /tmp, we cannot use it to know if we already ran or not
# This next line will attempt to cover this.
if (! pidof dockerd &>/dev/null && ! pidof ssh-agent &>/dev/null); then
    # clear /tmp
    rm -rf /tmp/* /tmp/.* 2>/dev/null
fi

# handle both ubuntu and alpine
if command -v apk 2>/dev/null 1>/dev/null; then
    # setup openrc
    if ! test -d /run/openrc || ! test -e /run/openrc/softlevel; then
        mkdir -p /run/openrc
        touch /run/openrc/softlevel
    fi

    # ensure docker
    if ! rc-service docker status &> /dev/null; then
        rc-status --servicelist
        rc-service docker start
    fi
else
    # ensure docker
    if ! service docker status > /dev/null; then
        service docker start
    fi
fi
