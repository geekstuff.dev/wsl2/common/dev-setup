#!make

.PHONY: .00-basics
.00-basics: .01-distro-upgrade.${LX_DISTRO} .02-basic-packages.${LX_DISTRO} .03-git-latest.${LX_DISTRO}
