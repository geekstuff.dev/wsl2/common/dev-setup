#!make

.PHONY: .01-distro-upgrade.ubuntu
.01-distro-upgrade.ubuntu:
	apt update
	apt upgrade -y
	apt dist-upgrade -y
	apt autoclean -y
	test -n "${WSL_DISTRO_NAME}" || rm -rf /var/lib/apt/lists/*
