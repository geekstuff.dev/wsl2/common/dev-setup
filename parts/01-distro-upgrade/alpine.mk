#!make

.PHONY: .01-distro-upgrade.alpine
.01-distro-upgrade.alpine:
	apk --no-cache --update upgrade
