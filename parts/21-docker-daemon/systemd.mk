#!make

.PHONY: .21-docker-daemon.setup.systemd
.21-docker-daemon.setup.systemd:
	mkdir -p /etc/docker
	cp parts/21-docker-daemon/systemd.docker-daemon.json /etc/docker/daemon.json
