#!make

.PHONY: .21-docker-daemon.setup.non-systemd
.21-docker-daemon.setup.non-systemd:
	mkdir -p /etc/docker
	cp parts/21-docker-daemon/non-systemd.docker-daemon.json /etc/docker/daemon.json
	! test "${LX_DISTRO}" = "ubuntu" || $(MAKE) .21-docker-daemon.setup.non-systemd.ubuntu

.PHONY: .21-docker-daemon.setup.non-systemd.ubuntu
.21-docker-daemon.setup.non-systemd.ubuntu:
	update-alternatives --set iptables /usr/sbin/iptables-legacy
	update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
