#!make

.PHONY: .21-docker-daemon.check
.21-docker-daemon.check:
	@! test -e /etc/docker/daemon.json || \
		{ echo "/etc/docker/daemon.json already exists"; false; }
