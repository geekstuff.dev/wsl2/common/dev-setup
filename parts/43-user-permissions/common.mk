#!make

.PHONY: .43-user-permissions
.43-user-permissions:
	chmod -R go-rwx \
		/home/${WSL_USER}/.ssh \
		/home/${WSL_USER}/.gnupg \
		/home/${WSL_USER}/.config
	chown -R ${WSL_USER}:${WSL_USER} \
		/home/${WSL_USER}/.ssh \
		/home/${WSL_USER}/.gnupg \
		/home/${WSL_USER}/.config \
		/home/${WSL_USER}/.hushlogin
