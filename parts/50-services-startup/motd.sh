#!/bin/sh

# fetch current docker-dev tag
DEV_SETUP_TAG=$(git -C /usr/lib/docker-dev tag --points-at HEAD 2>/dev/null)
if test -n "$DEV_SETUP_TAG"; then
    DEV_SETUP_SOURCE="https://gitlab.com/geekstuff.dev/wsl2/common/dev-setup/-/tree/${DEV_SETUP_TAG}"
else
    DEV_SETUP_SOURCE="%BUILD_SOURCE%"
fi

echo ""
echo "WSL2 Docker Dev [%BUILD_REF%, %SYSD_VARIANT%]"
echo "  build source: %BUILD_SOURCE%"
echo "  dev-setup source: $DEV_SETUP_SOURCE"
echo "  services: docker, ssh-agent, gpg-agent"
echo "  cli: \`docker-dev info\`"

# output once helpful bits about ssh and gpg if nothing is currently setup on those

NO_SSH_KEY=
if test $(find $HOME/.ssh/ -name 'id_*' | wc -l) -eq 0; then
    NO_SSH_KEY=1
fi

NO_GPG_KEY=
EMAIL=$(git config --global user.email)
if test -n "$EMAIL"; then
    if ! gpg -k $EMAIL 1>/dev/null 2>/dev/null; then
        NO_GPG_KEY=1
    fi
fi

# if keys exists, check if any are loaded
NO_SSH_KEY_LOADED=
if test -z "$NO_SSH_KEY"; then
    if ! ssh-add -l 1>/dev/null 2>/dev/null; then
        NO_SSH_KEY_LOADED=1
    fi
fi

if test -n "$NO_SSH_KEY" -o -n "$NO_SSH_KEY_LOADED" -o -n "$NO_GPG_KEY"; then
    # Show once this next message
    if ! test -e ${HOME}/.motd-extra-shown && test -n "$NO_SSH_KEY"; then
        echo "  . No ssh key exists. To create use \`ssh-keygen -t ed25519\` and choose a passphrase"
    fi
    # Show this message whenever it applies
    if test -n "$NO_SSH_KEY_LOADED"; then
        echo "  . No ssh key loaded. Run \`ssh-add\` to load your default key."
    fi
    # Show once this next message
    if ! test -e ${HOME}/.motd-extra-shown && test -n "$NO_GPG_KEY"; then
        echo "  . No gpg key exists for git signing. To get more info and easily setup, use \`docker-dev gpg\`"
    fi

    touch ${HOME}/.motd-extra-shown
fi

# Last empty line
echo ""
