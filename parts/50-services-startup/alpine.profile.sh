#!/bin/sh

cat <<'PROFILE' >> /home/$WSL_USER/.profile
# load bash completion
if test -e /etc/bash/bash_completion.sh; then
    source /etc/bash/bash_completion.sh
else
    source /etc/profile.d/bash_completion.sh
fi

# include .bashrc if it exists
if [ -n "$BASH_VERSION" ]; then
    if [ -f "$HOME/.bashrc" ]; then
        . "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

PROFILE
