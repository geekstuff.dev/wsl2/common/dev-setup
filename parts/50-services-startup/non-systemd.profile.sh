#!/bin/sh

cat <<'PROFILE' >> /home/$WSL_USER/.profile
### docker-dev (start) ###

# This next command ensures OS/global services (docker) are started. WSL actually
# implements a boot command to do this but it is not widely available at the moment.
sudo /usr/lib/docker-dev/bin/wsl-boot.non-systemd.sh 1>/dev/null 2>/dev/null \
  || echo "error: failed to launch docker-dev services [/usr/lib/docker-dev/bin/wsl-boot.non-systemd.sh]"

# ensure ssh-agent
check-ssh-agent() {
  [ -S "$SSH_AUTH_SOCK" ] && { ssh-add -l >& /dev/null || [ $? -ne 2 ]; }
}
check-ssh-agent || export SSH_AUTH_SOCK=/tmp/ssh-agent-$(id -u)
check-ssh-agent || { rm -f $SSH_AUTH_SOCK; eval "$(ssh-agent -s -a $SSH_AUTH_SOCK)" > /dev/null; }

# show motd once per startup
if ! test -e /tmp/.motd-shown; then
    run-parts /etc/update-motd.d/
    touch /tmp/.motd-shown
fi

### docker-dev (stop) ###
PROFILE

cat <<'BASHRC' >> /home/$WSL_USER/.bashrc
### docker-dev (start) ###

# gpg-agent
GPG_TTY=$(tty)
export GPG_TTY

### docker-dev (stop) ###
BASHRC
