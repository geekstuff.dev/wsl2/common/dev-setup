#!make

.PHONY: .50-services-startup.alpine
.50-services-startup.alpine:
	cp parts/50-services-startup/alpine.bashrc /home/${WSL_USER}/.bashrc
	parts/50-services-startup/alpine.profile.sh
	$(MAKE) .50-services-startup.${SYSD_VARIANT}
