#!make

.PHONY: .50-services-startup.check
.50-services-startup.check:
	! (test -e /home/${WSL_USER}/.profile && grep -q '### docker-dev (start) ###' /home/${WSL_USER}/.profile) \
	&& ! (test -e /home/${WSL_USER}/.bashrc && grep -q '### docker-dev (start) ###' /home/${WSL_USER}/.bashrc)

.PHONY: .50-services-startup.permissions
.50-services-startup.permissions:
	chown ${WSL_USER}: /home/${WSL_USER}/.bashrc /home/${WSL_USER}/.profile

.PHONY: .50-services-startup.motd
.50-services-startup.motd:
	mkdir -p /etc/update-motd.d
	cat parts/50-services-startup/motd.sh | \
		sed -e "s|%BUILD_REF%|${BUILD_REF}|" \
			-e "s|%BUILD_SOURCE%|${BUILD_SOURCE}|" \
			-e "s|%DOCKER_DEV_SERVICES%|${DOCKER_DEV_SERVICES}|" \
			-e "s|%SYSD_VARIANT%|${SYSD_VARIANT}|" \
		> /etc/update-motd.d/89-docker-dev
	chmod +x /etc/update-motd.d/89-docker-dev
	! test -e /etc/update-motd.d/60-unminimize || chmod -x /etc/update-motd.d/60-unminimize
