#!/bin/sh

cat <<'PROFILE' >> /home/$WSL_USER/.profile
### docker-dev (start) ###

# ssh-agent
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

# show motd once per startup
if ! test -e /tmp/.motd-shown; then
    run-parts /etc/update-motd.d/
    touch /tmp/.motd-shown
fi

### docker-dev (stop) ###
PROFILE

cat <<'BASHRC' >> /home/$WSL_USER/.bashrc
### docker-dev (start) ###

# gpg-agent
GPG_TTY=$(tty)
export GPG_TTY

### docker-dev (stop) ###
BASHRC
