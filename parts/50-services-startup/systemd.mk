#!make

.PHONY: .50-services-startup.systemd
.50-services-startup.systemd: .50-services-startup.systemd.profile
.50-services-startup.systemd: .50-services-startup.systemd.services

.PHONY: .50-services-startup.systemd.profile
.50-services-startup.systemd.profile:
	parts/50-services-startup/systemd.profile.sh

.PHONY: .50-services-startup.systemd.services
.50-services-startup.systemd.services:
	mkdir -p /home/${WSL_USER}/.config/systemd/user
	cp parts/50-services-startup/systemd.user.ssh-agent.service /home/${WSL_USER}/.config/systemd/user/ssh-agent.service
	chown -R ${WSL_USER}: /home/${WSL_USER}/.config
	sudo -u ${WSL_USER} systemctl --user enable ssh-agent
