#!make

.PHONY: .42-user-git-config.setup
.42-user-git-config.setup:
	cp parts/42-user-git-config/git.config /home/${WSL_USER}/.config/git/config
