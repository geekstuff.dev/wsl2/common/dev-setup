#!make

.PHONY: .03-git-latest.ubuntu
.03-git-latest.ubuntu:
	add-apt-repository ppa:git-core/ppa -y
	apt-get update
	apt-get -y install git
	test -n "${WSL_DISTRO_NAME}" || rm -rf /var/lib/apt/lists/*
