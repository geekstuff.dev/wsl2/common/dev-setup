#!make

.PHONY: .20-docker.setup.ubuntu
.20-docker.setup.ubuntu:
	/usr/bin/lsb_release -cs
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - 2>&1
	add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(shell lsb_release -cs) stable"

	@# Docker engine
	mkdir -p /etc/docker
	apt-get update
	DEBIAN_FRONTEND=noninteractive apt-get -y install --no-install-recommends docker-ce docker-ce-cli containerd.io docker-compose-plugin docker-buildx-plugin
	test -n "${WSL_DISTRO_NAME}" || rm -rf /var/lib/apt/lists/*

	@# Docker groups and permissions
	getent group 998 || groupadd -g 998 docker2
	usermod -aG docker ${WSL_USER}
	usermod -aG 998 ${WSL_USER}
	echo "${WSL_USER} ALL = NOPASSWD:/usr/sbin/service docker status, /usr/sbin/service docker start, /usr/lib/docker-dev/bin/wsl-boot.non-systemd.sh" > /etc/sudoers.d/sudouser
