#!make

.PHONY: .20-docker.check
.20-docker.check:
	@! command -v docker 1>/dev/null 2>/dev/null || \
		{ echo "docker already exists"; false; }
