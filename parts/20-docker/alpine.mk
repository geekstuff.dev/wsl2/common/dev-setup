#!make

.PHONY: .20-docker.setup.alpine
.20-docker.setup.alpine:
	apk --no-cache --update add docker docker-compose shadow docker-cli-buildx
	groupdel ping
	groupmod -g 998 docker
	groupadd -g 999 docker2
	usermod -aG docker ${WSL_USER}
	usermod -aG docker2 ${WSL_USER}
	sed -i 's/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/' /etc/sudoers
	echo "${WSL_USER} ALL = NOPASSWD:/bin/rc-status --servicelist, /bin/mkdir -p /run/openrc, /bin/touch /run/openrc/softlevel, /sbin/rc-service docker status, /sbin/rc-service docker start, /usr/lib/docker-dev/bin/wsl-boot.non-systemd.sh" > /etc/sudoers.d/sudouser
