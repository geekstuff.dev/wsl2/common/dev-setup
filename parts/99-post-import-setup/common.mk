#!make

# NOTE this must be copied to /usr/bin and not /usr/local/bin
.PHONY: .99-post-import-setup
.99-post-import-setup:
	cp parts/99-post-import-setup/post-import-setup.sh /usr/bin/post-import-setup
