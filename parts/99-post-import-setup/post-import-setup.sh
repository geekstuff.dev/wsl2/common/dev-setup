#!/bin/sh

# Exit on any failure
set -e

tmpUser="wsluser"

# Ensure root user
if [ $(id -u) -ne 0 ]; then
    echo "This script must be run with sudo"
    exit 1
fi

if test -e /root/.post-import-completed; then
    echo "post-import was already completed. cannot run twice."
    exit 1
fi

cat << MSG

#####################################
# WSL Docker-Dev: post-import-setup #
#####################################

This script finalizes your linux WSL2 docker-dev distribution.
It will configure your linux user, git name and git email.

MSG

# Request username
printf "%s" "> Select your username: "
read username

# Request full name
printf "%s" "> Enter your full name: "
read fullname

# Request email
printf "%s" "> Enter your email: "
read email

# Rename user
usermod -l "$username" -d "/home/$username" -m "$tmpUser"
usermod -c "$fullname" "$username"
groupmod -n "$username" "$tmpUser"
if command -v apt 1>/dev/null 2>/dev/null; then
    usermod -aG sudo $username
elif command -v apk 1>/dev/null 2>/dev/null; then
    usermod -aG wheel $username
fi

# passwd
printf "%s\n" "> Set your linux password"
passwd $username

# gitconfig
sudo -u $username git config --global user.name "$fullname"
sudo -u $username git config --global user.email "$email"

# sudoers
sed -i "s/$tmpUser/$username/g" /etc/sudoers.d/sudouser

# user ssh config
sed -i "s/$tmpUser/$username/g" /home/$username/.ssh/config

# wsl.conf
sed -i "s/default = root/default = $username/" /etc/wsl.conf
sed -i "s/# hostname = custom-hostname/hostname = $WSL_DISTRO_NAME/" /etc/wsl.conf

# Completed!
echo ""
touch /root/.post-import-completed
cat << MSG
#########################
# Post-import completed #
#########################

Your new docker dev WSL distro with batteries included is ready!

Optionally set this new WSL distribution as the default:
  wsl --setdefault $WSL_DISTRO_NAME

Otherwise start it up with:
  wsl ~ -d $WSL_DISTRO_NAME
MSG

# self-terminate
wsl.exe --terminate ${WSL_DISTRO_NAME}

# final space
echo ""
