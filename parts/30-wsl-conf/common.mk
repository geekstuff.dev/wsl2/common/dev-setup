#!make

.PHONY: .30-wsl-conf.check
.30-wsl-conf.check:
	@! test -e /etc/wsl.conf || \
		{ echo "/etc/wsl.conf already exists"; false; }
