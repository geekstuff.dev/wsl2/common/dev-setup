#!make

.PHONY: .30-wsl-conf.setup.systemd
.30-wsl-conf.setup.systemd:
	cp parts/30-wsl-conf/systemd.wsl.conf /etc/wsl.conf
