#!make

.PHONY: .30-wsl-conf.setup.non-systemd
.30-wsl-conf.setup.non-systemd:
	cp parts/30-wsl-conf/non-systemd.wsl.conf /etc/wsl.conf
