#!make

.PHONY: .10-wsl-user.setup.alpine
.10-wsl-user.setup.alpine:
	addgroup -g 1000 ${WSL_USER}
	adduser --disabled-password -s /bin/bash --uid 1000 -G ${WSL_USER} ${WSL_USER}
	mkdir -p /home/${WSL_USER}/.config
	chown ${WSL_USER}: /home/${WSL_USER}/.config
	chown -R ${WSL_USER}: /usr/lib/docker-dev
