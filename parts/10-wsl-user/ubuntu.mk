#!make

.PHONY: .10-wsl-user.setup.ubuntu
.10-wsl-user.setup.ubuntu:
	groupadd --gid 1000 ${WSL_USER}
	useradd -s /bin/bash --uid 1000 --gid 1000 -m ${WSL_USER}
	mkdir -p /home/${WSL_USER}/.config
	chown ${WSL_USER}: /home/${WSL_USER}/.config
	chown -R ${WSL_USER}: /usr/lib/docker-dev
