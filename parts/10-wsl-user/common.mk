#!make

.PHONY: .10-wsl-user.check
.10-wsl-user.check:
	@! grep -q ':1000:1000:' /etc/passwd || \
		{ echo "user 1000 already exists"; false; }
