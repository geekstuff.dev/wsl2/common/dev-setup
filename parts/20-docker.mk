#!make

.PHONY: .20-docker
.20-docker: .20-docker.check .20-docker.setup.${LX_DISTRO}
.20-docker: .21-docker-daemon.check .21-docker-daemon.setup.${SYSD_VARIANT}
