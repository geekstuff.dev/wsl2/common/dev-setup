#!make

.PHONY: .50-wsl-services
.50-wsl-services: .50-services-startup.check
.50-wsl-services: .50-services-startup.${LX_DISTRO}
.50-wsl-services: .50-services-startup.permissions
.50-wsl-services: .50-services-startup.motd
