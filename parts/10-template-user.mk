#!make

.PHONY: .10-template-user
.10-template-user: .10-wsl-user.fix.ubuntu-2204 .10-wsl-user.check .10-wsl-user.setup.${LX_DISTRO}

.PHONY: .10-wsl-user.fix.ubuntu-2204
.10-wsl-user.fix.ubuntu-2204:
	test -z "$(shell grep -s 'ubuntu:x:1000:1000' /etc/passwd)" \
		|| userdel --force --remove ubuntu
