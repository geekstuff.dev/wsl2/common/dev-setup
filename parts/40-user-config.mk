#!make

.PHONY: .40-user-config
.40-user-config: .40-user-folders.setup
.40-user-config: .41-user-ssh-config.setup
.40-user-config: .42-user-git-config.setup
.40-user-config: .43-user-permissions
.40-user-config: .44-user-tools
