#!make

.PHONY: .02-basic-packages.alpine
.02-basic-packages.alpine:
	apk --update --no-cache add \
		bash-completion \
		ca-certificates \
		curl \
		git \
		git-bash-completion \
		gnupg \
		iproute2 \
		jq \
		less \
		libstdc++ \
		make \
		nano \
		openrc \
		openssh-client \
		openssl \
		procps \
		socat \
		sudo \
		tree \
		unzip \
		wget
	mkdir -p /etc/bash_completion.d
	@# In recent Alpine, 3.20 at least, an "export PATH=..." is present in /etc/profile and blocks WSL Windows Paths.
	@# Previous Alpine don't have this and works fine, and this fixes it and the commented paths still appears fine.
	sed -i 's/^export PATH/#export PATH/' /etc/profile
