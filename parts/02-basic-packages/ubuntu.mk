#!make

.PHONY: .02-basic-packages.ubuntu
.02-basic-packages.ubuntu:
	apt-get update
	apt-get -y install --no-install-recommends \
		apt-transport-https \
		apt-utils \
		dialog \
		2>&1
	apt-get -y install \
		bash-completion \
		ca-certificates \
		curl \
		gnupg \
		gnupg-agent \
		gpg \
		iproute2 \
		jq \
		less \
		lsb-release \
		make \
		nano \
		net-tools \
		openssh-client \
		procps \
		socat \
		software-properties-common \
		sudo \
		tree \
		unzip \
		wget
	test -n "${WSL_DISTRO_NAME}" || rm -rf /var/lib/apt/lists/*
