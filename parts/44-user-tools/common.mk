#!make

.PHONY: .44-user-tools
.44-user-tools:
	rm -f /usr/local/bin/devcontainer-open
	ln -s $(shell pwd)/bin/devcontainer-open.sh /usr/local/bin/devcontainer-open
