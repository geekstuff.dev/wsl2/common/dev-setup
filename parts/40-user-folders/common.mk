#!make

.PHONY: .40-user-folders.setup
.40-user-folders.setup:
	touch /home/${WSL_USER}/.hushlogin
	mkdir -p \
		/home/${WSL_USER}/.ssh \
		/home/${WSL_USER}/.gnupg \
		/home/${WSL_USER}/.config/git
	chmod -R go-rwx \
		/home/${WSL_USER}/.ssh \
		/home/${WSL_USER}/.gnupg \
		/home/${WSL_USER}/.config
	chown -R ${WSL_USER}:${WSL_USER} \
		/home/${WSL_USER}/.ssh \
		/home/${WSL_USER}/.gnupg \
		/home/${WSL_USER}/.config \
		/home/${WSL_USER}/.hushlogin
