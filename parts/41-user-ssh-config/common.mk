#!make

.PHONY: .41-user-ssh-config.setup
.41-user-ssh-config.setup:
	cp parts/41-user-ssh-config/ssh.config /home/${WSL_USER}/.ssh/config
