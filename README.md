# WSL2 / Common / Dev-Setup

This project configures a Linux distributions as a **Docker Dev** image for usage in WSL2,
along with an embedded docker engine and a few basic tools for IT development purposes.

In minutes users can get up and running by importing those pre-built images in WSL
and start working on their IT projects and containers right away, using 99.9%
the Linux way.

- A few pre-built linux distributions are available [here](https://gitlab.com/geekstuff.dev/wsl2/docker-dev/)
- And see there Deployments / Releases section for the latest generate quick install
  instructions such as the one for the [Alpine image](https://gitlab.com/geekstuff.dev/wsl2/docker-dev/alpine/-/releases)

## Features

- A docker engine, started either on user login or through systemd depending
- Docker compose, ssh-agent, gpg-agent, latest git
- Starting configs for ssh, git and gpg
- A `docker-dev info` for users to easily do more
- Compatible with VScode running in WSL
- Compatible with Devcontainers
- Compatible with any Linux tools that creates local k8 clusters
- Lastly a copy of this project

## Target linux distributions

- Ubuntu 20.04
- Ubuntu 22.04
- Ubuntu 22.04 with systemd*
- Ubuntu 24.04 with systemd*
- Alpine (3.*)

Each of those linux distributions are built in their own projects here:
[https://gitlab.com/geekstuff.dev/wsl2/docker-dev/](https://gitlab.com/geekstuff.dev/wsl2/docker-dev/)

They each copy this common/dev-setup project inside their image and execute its
code to build and package that linux distribution.

See each project "Releases" section and their latest generated instructions
to easily import your new WSL image.

NOTE: WSL with systemd enabled is not yet available on all Windows using WSL2.
      See below for more info.

## Status of the project

Stable and looking good.

- Code moved from each docker-dev images to this project is working as before.
  cleanup will be needed and will greatly simplify that part.
- User output is much more simplified now.
- Gpg setup is extra automated. Should be a quick piece of cake for anyone.
- User driven [utilities](./utilities) are being added for k8 and more.
- READMEs / Documentation needs some work

## Launching VSCode in WSL mode

Visual Studio Code is able to run inside a WSL distribution.

This enables it to work right next to a docker engine and any other needed
linux backends, and even work within docker containers launched in your
WSL distribution.

- Install VSCode
- Install the [Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack) extension
- Start shell inside your new WSL distro
- Run `code -n ~/`

With the extension installed, the bottom left green rectangle in VSCode can also be used
to switch in which environment VSCode runs in. Your windows host, a specific WSL distro, etc.

## Test out VSCode devcontainers

VSCode dev containers uses the Remote Development extension installed above, and
lets you define a containerized development environment for a project.

Let's use a Rust cli hello world app to test this out:

- Start shell inside your new WSL distro
- `git clone https://gitlab.com/geekstuff.dev/playground/rust-cli.git`
- `code -n rust-cli/`
- In VSCode, a dialog box should appear to "Reopen in container"
  - If those dialog boxes are muted, you can hit F1 and type "Reopen in container"
- Once the container is done building, a terminal will automatically appear inside VSCode,
  and it will build and run the Rust app automatically.

TIPs:

- You can do the same thing with this current project which also holds a devcontainer
  that is setup to build and package this WSL2 image.
- Also checkout this [Awesome Dev Containers list](https://github.com/manekinekko/awesome-devcontainers)

## Create a local Kubernetes cluster inside your WSL

In WSL you can use any linux instructions you find to achieve this.

Some of those instructions are embedded into scripts and copied inside your imported
WSL image, for you to use if you want to.

In a shell/terminal inside your WSL image, use `docker-dev k8` for info on what
is available.

## How to remove an imported distro

Here is an example removal:

- `wsl --unregister ubuntu-dev`
- `rm .\Documents\wsl-ubuntu-dev`

## Requirements for Systemd integration in WSL

The systemd parts of this setup is built around the recently announced systemd integration
of WSL. This information is accurate as of `2022-12-15` but may get updated once that
WSL version is not in preview anymore.

Systemd support requires WSL2, and also requires to install the `WSL Preview` update, which
enables commands like `wsl --version`.

For Windows 11 users, there are a few easy options to get it:

- In settings, windows update, advanced options: Turn on `Receive updates for other Microsoft products`
  - This is the ideal option to automatically get future WSL updates.
  - Might not be allowed by organization policies
- `wsl --update`
- Through the [WSL store page](https://aka.ms/wslstorepage).

For Windows 10 users, you need to opt-in to the `Insider program` in your Windows settings and
then update everything once or twice.

To know if your WSL is updated enough, just see if the command `wsl --version` works.

## Systemd basics

- List system services: `systemctl list-unit-files --type=service`
- List user services: `systemctl list-unit-files --type=service --user`
- View the log of a systemd unit: `sudo journalctl -u docker`
- Add your own user service: See how this is done here for ssh-agent in the [Dockerfile](./wsl-image/Dockerfile)

More references:

- [Digital Ocean - Systemd Essentials](https://www.digitalocean.com/community/tutorials/systemd-essentials-working-with-services-units-and-the-journal)
- [Archlinux Wiki - Systemd](https://wiki.archlinux.org/title/systemd)

## FAQ

Can I re-use my ssh key instead of having a new one in WSL?
> Yes but every single git registries supports multiple ssh keys, so there are no valid
> reason to re-use the same one in many places. Although if it's the one from your windows
> environment that you are already using for something else, that could be different.

Am I am able to get updates if the image here is updated?
> It's in progress. Creating the common/dev-setup repo is part of it, and there are
> more parts needed.

I noticed an issue with my devcontainer where no extensions get installed, nothing happens in it.
> It seems to be happening only for users of devcontainers + WSL/systemd version, where in the
> extensions tab there are 0 local, dev container or recommended extensions. Flickering between
> Rebuild container and Rebuild container without cache seems to do something.
>
> Another reason where this can happen is if you have extensions defined in both
> `.vscode/settings.json` and in `.devcontainer/devcontainer.json`. In a devcontainer
> build, it seems like sometimes it chooses the first extension list instead of the second.

Systemd?
> Recently MS has announced it's coming up. It is available through the Insider program for now
> or if you are using Windows 11 and install WSL preview through the windows store.
> See the dedicated section above for more info.

For ssh-agent, why not the Windows SSH agent service?
> This was initially the choice but that service is NOT the open source version as it name wants
> to imply. For instance it ignores keys loading TTL, and also saves ssh keys passphrases
> forever in Windows registry, beating the purpose of ssh key passphrases entirely. Also by
> using linux ssh-agent we enjoy way more reliability, security and stability over time
> without any unintuitive, obscure, insecure or stupid changes being made behind the scene.
