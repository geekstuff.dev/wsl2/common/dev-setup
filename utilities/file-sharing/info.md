# WSL to WSL file sharing

This module let you setup one WSL distribution as a file server acting behind the scene,
and it easily lets you setup any number of WSL clients that easily gets setup to use the
file server and mount it's resources

## For server setup

- A WSL distro with systemd enabled gets imported for a file "server" role. Example distro:
  https://gitlab.com/geekstuff.dev/wsl2/docker-dev/ubuntu-22.04-systemd/-/releases
- That distro gets setup to share/expose its folders: ~/.ssh, ~/.config/git, ~/.gnupg, ~/shared
  - First thing is to ensure that you generate an ssh key and ideally a gpg key.
    - `ssh-keygen -t ed25519`
    - `docker-dev gpg`
- ~/shared folder is meant to let user create folders in it and have each of
  them mounted in client WSL distro ~/ HOME folder.
  - Clients can also choose to be more specific in which folders gets mounted in their HOME

To install the server components:

```
docker-dev file-sharing.server-install
```

Note: You can also use "file-sharing.server-uninstall"

## For clients setup (debian based or alpine, with or without systemd)

- On a system where a WSL distro is already configured as a file server, a utility is available
  to configure the client to ensure server gets up if not already up and then mount folders.
- Launch a terminal in your WSL distro, debian based or alpine, with systemd or not.
  variety of compatible WSL distro here: https://gitlab.com/geekstuff.dev/wsl2/docker-dev/
- To configure, start the client installer that the server distro is already sharing:

The command to install the client components uses make and depends on which WSL distro
is the server.

```
make -C /mnt/wsl/<server-wsl-distro-name>/client install
```

Note: "uninstall" can also be used instead.
