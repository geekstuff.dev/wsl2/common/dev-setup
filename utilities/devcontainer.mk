#!make

.PHONY: devcontainer
devcontainer:
	$(info Devcontainer Info)
	$(info )
	$(info This WSL distro is compatible with VSCode devcontainers.)
	$(info )
	$(info It also includes a utility to launch a devcontainer directly without)
	$(info having to first open VSCode and then use "reopen in devcontainer" button or action.)
	$(info )
	$(info Simply use \`devcontainer-open x\`, with x being either a single)
	$(info project with a .devcontainer/ folder inside it, or x being a)
	$(info "*.code-workspace" file.)
	$(info )
	$(info You can also create windows shortcuts using the devcontainer-open tool:)
	$(info `wsl.exe -d my-wsl-distro devcontainer-open ~/devcontainer/path`)
	@:
