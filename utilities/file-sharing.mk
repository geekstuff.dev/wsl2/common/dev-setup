.PHONY: file-sharing
file-sharing:
	@cat utilities/file-sharing/info.md

.PHONY: file-sharing.server-install
file-sharing.server-install:
	@$(MAKE) -C utilities/file-sharing/server install

.PHONY: file-sharing.server-uninstall
file-sharing.server-uninstall:
	@$(MAKE) -C utilities/file-sharing/server uninstall
