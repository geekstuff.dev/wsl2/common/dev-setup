#!make

.PHONY: k8-kind
k8-kind:
	@sed -e "s/%CLI_COMMAND%/${CLI_COMMAND}/g" utilities/k8/k8-kind.md

.PHONY: k8-kind.cli
k8-kind.cli: .k8-kind.cli.install .k8-kind.cli.install-autocompletion

.PHONY: .k8-kind.cli.install
.k8-kind.cli.install:
	@printf "> ensure kind cli: "
	@command -v kind 2>/dev/null 1>/dev/null && printf "ok\n" || ( \
		printf "installing..\n" \
		&& curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.17.0/kind-linux-amd64 \
		&& chmod +x ./kind \
		&& sudo mv ./kind /usr/local/bin/kind \
	)

.PHONY: .k8-kind.cli.install-autocompletion
.k8-kind.cli.install-autocompletion:
	@printf "> kind auto completion: "
	@test -e /etc/bash_completion.d/kind && printf "ok\n" || ( \
		printf "installing..\n" \
		&& kind completion bash | sudo tee /etc/bash_completion.d/kind 1>/dev/null \
		&& echo "restart shell or \`source ~/.profile\` to load autocompletion" \
	)

.PHONY: .k8-kind.cli.uninstall-autocompletion
.k8-kind.cli.uninstall-autocompletion:
	@! test -e /etc/bash_completion.d/kind || sudo rm /etc/bash_completion.d/kind

###########################

# NOTE: KinD will create following kubeconfig with X cluster name:
#         cluster name: kind-X
#         context name: X
#         user name: kind-X
K8_KIND_CLUSTER_NAME ?= dev

.PHONY: k8-kind.start
k8-kind.start:
	@printf "ensure kind cluster: " && $(MAKE) .k8-kind.is-up 2>/dev/null && printf "ok\n" || ( \
		printf "starting..\n" \
		&& $(MAKE) .k8-kind.create \
		&& while [ 1 -eq 1 ]; do if $(MAKE) .k8-kind.is-up 1>/dev/null 2>/dev/null; then break; fi; sleep 1; done \
		&& kind get kubeconfig --name ${K8_KIND_CLUSTER_NAME} --internal > ~/.kube/kind.config.internal \
	)
	@$(MAKE) .k8-kind.kubeconfig.default-context

.PHONY: k8-kind.delete
k8-kind.delete:
	@printf "delete kind cluster: "
	@kind delete cluster --name ${K8_KIND_CLUSTER_NAME} 1>/dev/null 2>/dev/null
	@printf "ok\n"

.PHONY: .k8-kind.is-up
.k8-kind.is-up:
	@kind get kubeconfig --name ${K8_KIND_CLUSTER_NAME} 2>/dev/null 1>/dev/null

# TODO in some cases, after already having created a cluster and getting back to this project,
#      the kind cluster already exists, .kind-create passes, but later usage of kubectl fails.
#      Ideally this scenario would be detected here and cluster deleted + recreated for now.
.PHONY: .k8-kind.create
.k8-kind.create:
	kind create cluster \
		--name ${K8_KIND_CLUSTER_NAME} \
		--wait 5m \
		--config $(shell pwd)/utilities/k8/k8-kind.yaml

.PHONY: .k8-kind.kubeconfig.default-context
.k8-kind.kubeconfig.default-context:
	@kubectl config set-context kind-${K8_KIND_CLUSTER_NAME} 1>/dev/null 2>/dev/null
