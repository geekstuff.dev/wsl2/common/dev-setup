#!make

.PHONY: k8-cli
k8-cli: .k8-cli.kubectl.install.${LX_BASE} .k8-cli.kubectl.install-autocompletion
k8-cli: .k8-cli.helm.install .k8-cli.helm.install-autocompletion

.PHONY: .k8-cli.kubectl.install.debian
.k8-cli.kubectl.install.debian:
	@printf "> ensure kubectl cli: "
	command -v kubectl 2>/dev/null 1>/dev/null && printf "ok\n" || ( \
		printf "installing..\n" \
		&& sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg \
		&& echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list \
		&& sudo apt-get update \
		&& sudo apt-get install -y kubectl \
	)

.PHONY: .k8-cli.kubectl.install.alpine
.k8-cli.kubectl.install.alpine:
	@printf "> ensure kubectl cli: "
	command -v kubectl 2>/dev/null 1>/dev/null && printf "ok\n" || ( \
		printf "installing..\n" \
		&& curl -LO https://storage.googleapis.com/kubernetes-release/release/$(shell curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl \
		&& chmod +x kubectl \
		&& sudo mv kubectl /usr/local/bin/kubectl \
	)

.PHONY: .k8-cli.helm.install
.k8-cli.helm.install:
	@printf "> ensure helm cli: "
	command -v helm 2>/dev/null 1>/dev/null && printf "ok\n" || ( \
		printf "installing..\n" \
		&& curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash \
	)

.PHONY: .k8-cli.kubectl.install-autocompletion
.k8-cli.kubectl.install-autocompletion:
	@printf "> kubectl auto completion: "
	test -e /etc/bash_completion.d/kubectl && printf "ok\n" || ( \
		printf "installing..\n" \
		&& kubectl completion bash | sudo tee /etc/bash_completion.d/kubectl 1>/dev/null \
		&& echo "restart shell or \`source ~/.profile\` to load autocompletion" \
	)

.PHONY: .k8-cli.kubectl.uninstall-autocompletion
.k8-cli.kubectl.uninstall-autocompletion:
	! test -e /etc/bash_completion.d/kubectl || sudo rm /etc/bash_completion.d/kubectl

.PHONY: .k8-cli.helm.install-autocompletion
.k8-cli.helm.install-autocompletion:
	@printf "> helm auto completion: "
	test -e /etc/bash_completion.d/helm && printf "ok\n" || ( \
		printf "installing..\n" \
		&& helm completion bash | sudo tee /etc/bash_completion.d/helm 1>/dev/null \
		&& echo "restart shell or \`source ~/.profile\` to load autocompletion" \
	)

.PHONY: .k8-cli.helm.uninstall-autocompletion
.k8-cli.helm.uninstall-autocompletion:
	! test -e /etc/bash_completion.d/helm || sudo rm /etc/bash_completion.d/helm
