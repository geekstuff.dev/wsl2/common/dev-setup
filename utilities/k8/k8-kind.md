# Geekstuff.dev / WSL2 / Common / Dev-Setup / Kubernetes / KinD

This utility aims to make it easy to create and use a local kubernetes
cluster, using [KinD](https://kind.sigs.k8s.io/).

> %CLI_COMMAND% k8-kind                     this help message
> %CLI_COMMAND% k8-kind.cli                 installs "kind" cli and autocomplete
> %CLI_COMMAND% k8-kind.start               starts a 3 node local k8 cluster "dev"
> %CLI_COMMAND% k8-kind.delete              deletes a local k8 cluster "dev"
