#!make

.PHONY: k8-kubefwd
k8-kubefwd:
	@sed -e "s/%CLI_COMMAND%/${CLI_COMMAND}/g" utilities/k8/k8-kubefwd.md

.PHONY: k8-kubefwd.cli
k8-kubefwd.cli: .k8-kubefwd.cli.install .k8-kubefwd.cli.install-autocompletion

.PHONY: .k8-kubefwd.cli.install
.k8-kubefwd.cli.install: TARBALL_URL=https://github.com/txn2/kubefwd/releases/download/1.22.4/kubefwd_Linux_x86_64.tar.gz
.k8-kubefwd.cli.install: TARBALL_DEST=/tmp/kubefwd.tar.gz
.k8-kubefwd.cli.install: EXTRACT_DIR=/tmp/kubefwd
.k8-kubefwd.cli.install:
	@printf "> ensure kubefwd cli: "
	command -v kubefwd 2>/dev/null 1>/dev/null && printf "ok\n" || ( \
		printf "installing..\n" \
		&& rm -rf ${TARBALL_DEST} ${EXTRACT_DIR} \
		&& mkdir -p ${EXTRACT_DIR} \
		&& curl --fail -L -o "$(TARBALL_DEST)" "${TARBALL_URL}" \
		&& tar zxf "$(TARBALL_DEST)" -C "$(EXTRACT_DIR)" \
		&& sudo mv ${EXTRACT_DIR}/kubefwd /usr/local/bin/ \
    	&& rm -rf $(TARBALL_DEST) $(EXTRACT_DIR) \
	)

.PHONY: .k8-kubefwd.cli.install-autocompletion
.k8-kubefwd.cli.install-autocompletion:
	@printf "> kubefwd auto completion: "
	@test -e /etc/bash_completion.d/kubefwd && printf "ok\n" || ( \
		printf "installing..\n" \
		&& kubefwd completion bash | sudo tee /etc/bash_completion.d/kubefwd 1>/dev/null \
		&& echo "restart shell or \`source ~/.profile\` to load autocompletion" \
	)

.PHONY: .k8-kubefwd.cli.uninstall-autocompletion
.k8-kubefwd.cli.uninstall-autocompletion:
	@! test -e /etc/bash_completion.d/kubefwd || sudo rm /etc/bash_completion.d/kubefwd

###

.PHONY: .k8-kubefwd.test-k8
.k8-kubefwd.test-k8:
	@kubectl cluster-info 1>/dev/null 2>/dev/null || false

.PHONY: .k8-kubefwd.test-no-kubefwd
.k8-kubefwd.test-no-kubefwd:
	@! pidof kubefwd 1>/dev/null 2>/dev/null || false

.PHONY: k8-kubefwd.start
k8-kubefwd.start:
	@while [ 1 -eq 1 ]; do if $(MAKE) .k8-kubefwd.test-k8 2>/dev/null; then break; \
		else echo "k8 cluster not found. Did you forget to start KinD cluster?"; fi; sleep 2; done
	@while [ 1 -eq 1 ]; do if $(MAKE) .k8-kubefwd.test-no-kubefwd 2>/dev/null; then break; \
		else echo "kubefwd is already running elsewhere"; fi; sleep 2; done
	sudo -E kubefwd services -n default

.PHONY: k8-kubefwd.stop
k8-kubefwd.stop:
	@$(MAKE) .k8-kubefwd.test-no-kubefwd 2>/dev/null || ( \
		sudo kill $(shell pidof kubefwd) \
		&& touch /tmp/kubefwd.stop \
	)

.PHONY: k8-kubefwd.constant-start
k8-kubefwd.constant-start:
	@while [ 1 -eq 1 ]; do \
		if test -e /tmp/kubefwd.stop; then \
			echo "Stopping because /tmp/kubefwd.stop is present."; \
			echo "Remove that file to resume k8-kubefwd.constant-start."; \
		else \
			$(MAKE) k8-kubefwd.start; \
		fi; \
		echo ".. restarting kubefwd loop in 3 seconds"; \
		sleep 3; \
	done
