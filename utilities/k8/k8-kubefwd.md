# Geekstuff.dev / WSL2 / Common / Dev-Setup / Kubernetes / Kubefwd

Kubefwd creates tunnels between your linux instance and a kubernetes cluster,
using your kubeconfig. It makes all the k8 services in that cluster, available
locally using the same in-cluster names.

In other words if you create a k8 deployment of an nginx pod, and a k8 service
named "my-service" that will forward network traffic to your deployment, once
you start kubefwd pointing to a k8 namespace, you will be able to locally use
`curl http://my-service`.

> %CLI_COMMAND% k8-kubefwd                  this help message
> %CLI_COMMAND% k8-kubefwd.cli              installs "kubefwd" cli and autocomplete
> %CLI_COMMAND% k8-kubefwd.start            starts tunnels to k8 clusters default namespace
> %CLI_COMMAND% k8-kubefwd.stop             stops kubefwd tunnels
> %CLI_COMMAND% k8-kubefwd.constant-start   runs kubefwd start in a constant loop
