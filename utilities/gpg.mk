#!make

GPG_PINENTRY_APP ?= /mnt/c/Program Files (x86)/Gpg4win/bin/pinentry.exe
GPG_EMAIL = $(shell git config --global user.email)
GPG_NAME = $(shell git config --global user.name)

.PHONY: gpg
gpg:
	$(info GPG Git commit signing.)
	$(info )
	$(info This utility can generate a GPG signing key for you if you don't have one already,)
	$(info and setup your git configuration to sign commits with it.)
	$(info )
	$(info > ${CLI_COMMAND} gpg-setup           configures gpg signing key and git)
	$(info > ${CLI_COMMAND} gpg-export          output public portion of gpg key)
	$(info > ${CLI_COMMAND} gpg-list            list gpg keys)
	$(info > ${CLI_COMMAND} gpg-disable         disable git commit signing)
	$(info > ${CLI_COMMAND} gpg-basics          configures gpg-agent and basics only. (special purpose))
	@:

.PHONY: gpg-setup
gpg-setup:
	$(info > Ensure gpg git signing.)
	@$(MAKE) gpg-basics
	@printf "> About to generate gpg key. Press Enter to continue or CTRL+C to stop here: "
	@read useless
	@$(MAKE) .gpg-ensure-key
	@$(MAKE) .gpg-ensure-git
	@echo "> Gpg setup ensured. WSL image ready."
	@echo "> Use this command to validate gpg signing: \`echo hello | gpg --clearsign\`"

.PHONY: gpg-basics
gpg-basics:
	$(info > Ensure gpg basics (checks and agent config))
	@$(MAKE) .gpg-checks
	@$(MAKE) .gpg-ensure-agent-conf

.PHONY: .gpg-checks
.gpg-checks:
	@$(MAKE) .gpg-check-cli 2>/dev/null || ( \
		echo "error: gpg cli is not installed. Check with your package manager." \
		&& false \
	)
	@$(MAKE) .gpg-check-pinentry-app 2>/dev/null || ( \
		echo "error: Could not find gpg GPG_PINENTRY_APP at ${GPG_PINENTRY_APP}." \
		&& echo "If it is available somewhere else, prefix this command with GPG_PINENTRY_APP=windows-pinentry-app" \
		&& echo "Otherwise download and install gpg4win https://www.gpg4win.org/" \
		&& echo "Its core functionality comes with a valid pinentry app which this" \
		&& echo "app will use if at /mnt/c/Program Files (x86)/Gpg4win/bin/pinentry.exe" \
		&& echo "or overriden with GPG_PINENTRY_APP env variable." \
		&& false \
	)
	@$(MAKE) .gpg-check-userinfo 2>/dev/null || ( \
		echo "error: could not get user.name or user.email from git config" \
		&& false \
	)

.PHONY: .gpg-ensure-key
.gpg-ensure-key:
	@$(MAKE) .gpg-check-has-signingkey 2>/dev/null || ( \
		echo "> Creating RSA 4096 GPG signing key. You will be asked to choose a passphrase" \
		&& $(MAKE) .gpg-create 1>/dev/null 2>/dev/null \
		&& echo "> Key Created!" \
		&& echo "Use \`${CLI_COMMAND} gpg-export\` to get the gpg public key and upload it to" \
		&& echo "github, gitlab, or wherever the git code is hosted on." \
	)

.PHONY: .gpg-ensure-git
.gpg-ensure-git:
	@$(MAKE) .gpg-check-git-config 2>/dev/null || ( \
		echo "> Ensure git config is setup to use the gpg signing key" \
		&& $(MAKE) .gpg-ensure-git-config \
	)

.PHONY: .gpg-check-cli
.gpg-check-cli:
	@command -v gpg 1>/dev/null

.PHONY: .gpg-check-pinentry-app
.gpg-check-pinentry-app:
	@test -e "${GPG_PINENTRY_APP}"

.PHONY: .gpg-check-userinfo
.gpg-check-userinfo:
	@test -n "${GPG_EMAIL}"
	@test -n "${GPG_NAME}"

.PHONY: .gpg-ensure-agent-conf
.gpg-ensure-agent-conf:
	@test -e ${HOME}/.gnupg || mkdir -p ${HOME}/.gnupg
	@test -e ${HOME}/.gnupg/gpg-agent.conf || touch ${HOME}/.gnupg/gpg-agent.conf
	@grep -q "pinentry-program" ${HOME}/.gnupg/gpg-agent.conf \
		|| echo "pinentry-program ${GPG_PINENTRY_APP}" >> ${HOME}/.gnupg/gpg-agent.conf
	@chmod -R go-rwx ${HOME}/.gnupg
	@kill $(shell pidof gpg-agent 2>/dev/null) 2>/dev/null || true
	@echo "> gpg-agent configured."

.PHONY: .gpg-check-has-signingkey
.gpg-check-has-signingkey:
	@test -n "$(shell $(MAKE) .gpg-get-key-id 2>/dev/null)"

.PHONY: .gpg-get-key-id
.gpg-get-key-id:
	@gpg --list-secret-keys --keyid-format LONG ${GPG_EMAIL} 2>/dev/null | egrep '\[(SC|S)\]' | cut -d'/' -f2 | cut -d' ' -f1

.PHONY: .gpg-check-git-config
.gpg-check-git-config:
	@test -n "$(shell git config --global user.signingkey)"
	@test "$(shell git config --global commit.gpgsign)" = "true"

.PHONY: .gpg-ensure-git-config
.gpg-ensure-git-config:
	@git config --global commit.gpgsign true
	@git config --global user.signingkey $(shell $(MAKE) .gpg-get-key-id)

.PHONY: gpg-export
gpg-export:
	gpg --armor --export ${GPG_EMAIL}
	@echo ""
	@echo ">> NOTE: The public key is all the lines above including the first and last"
	@echo ">>       lines with -----BEGIN and END PGP etc.-----"

.PHONY: .gpg-create
.gpg-create:
	@sed \
		-e "s|%EMAIL%|${GPG_EMAIL}|" \
		-e "s|%NAME%|${GPG_NAME}|" \
		-e "s|%COMMENT%|From WSL on $(shell powershell.exe echo \$$env:computername)|" \
		utilities/gpg.create.batch \
	| tee /dev/null \
	| gpg --batch --generate-key

.PHONY: gpg-list
gpg-list:
	@gpg --list-secret-keys --keyid-format SHORT ${GPG_EMAIL}

.PHONY: gpg-disable
gpg-disable:
	@git config --global commit.gpgsign false
