#!make

DOCKER_FLUSH_ITERATIONS ?= 3

.PHONY: docker-flush
docker-flush: ITERATIONS=$(shell seq 1 ${DOCKER_FLUSH_ITERATIONS})
docker-flush:
	@echo "> This will run ${DOCKER_FLUSH_ITERATIONS} times to flush all of your docker containers,"
	@printf "> images, networks and volumes. Press Enter to continue or CTRL+C to stop here: "
	@read useless
	@for i in ${ITERATIONS}; do \
		echo "> run no. $${i}"; \
		$(MAKE) .docker-flush; \
	done

.PHONY: .docker-flush
.docker-flush:
	docker system prune -af --volumes
	docker builder prune -af
	docker container prune -f
	docker image prune -af
	docker network prune -f
	docker volume prune -af
