#!make

include utilities/k8/*.mk

.PHONY: k8
k8:
	$(info Kubernetes related helpers)
	$(info )
	$(info > ${CLI_COMMAND} k8-cli              installs kubectl and helm)
	$(info > ${CLI_COMMAND} k8-kind             installs KinD cli and lets you start/stop a local cluster)
	$(info > ${CLI_COMMAND} k8-kubefwd          installs kubefwd and can start/stop an instance)
	@:
