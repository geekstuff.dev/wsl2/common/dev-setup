#!make

# NOTE Eventually make this easier
.PHONY: static-dns
static-dns:
	$(info Static DNS instructions)
	$(info )
	$(info If you use a VPN, you will get into a few regular networking issues)
	$(info with how things currently work by default inside WSL2.)
	$(info )
	$(info Setting static DNS entries inside WSL solves this.)
	$(info )
	$(info This command at the moment only provides additional instructions)
	$(info to the accurate instructions here: https://superuser.com/questions/1533291/how-do-i-change-the-dns-settings-for-wsl2)
	$(info )
	$(info > For a personnal WSL distro on a computer that uses VPN other times,)
	$(info You will want to set your home DNS or public dns like google or cloudflare.)
	$(info )
	$(info > For a work WSL distro with VPN, your resolv.conf should look like the following:)
	$(info nameserver A.B.C.D)
	$(info nameserver A.B.C.E)
	$(info search company-domain.org)
	@:
